FROM openjdk:11-jdk-slim
ADD /target/*.jar springbootgitlab-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/springbootgitlab-0.0.1-SNAPSHOT.jar"]

